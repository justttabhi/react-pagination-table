import React, { useEffect, useState } from "react";
import getData from "./tabledata";
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

function Table() {
    const [tableData, setTableData] = useState([]); // Assuming 'data' is your original array
    const [itemsPerPage, setItemsPerPage] = useState(15);// Adjust as needed
    const [currentPage, setCurrentPage] = useState(1);
    const [filteredData, setFilteredData] = useState([]); // Assuming 'data' is your original array
    const [showFilter, setshowFilter] = useState('')

    function formatDate(inputDate) {
        const dateObject = new Date(inputDate);
        const day = String(dateObject.getDate()).padStart(2, '0');
        const month = String(dateObject.getMonth() + 1).padStart(2, '0');
        const year = dateObject.getFullYear();

        return `${day}-${month}-${year}`;
    }

    /* 
        function formatDateWithTime(inputDate) {
           const dateObject = new Date(inputDate);
           const day = String(dateObject.getDate()).padStart(2, '0');
           const month = String(dateObject.getMonth() + 1).padStart(2, '0');
           const year = dateObject.getFullYear();
           const hours = String(dateObject.getHours()).padStart(2, '0');
           const minutes = String(dateObject.getMinutes()).padStart(2, '0');
           const seconds = String(dateObject.getSeconds()).padStart(2, '0');
           return `${day}-${month}-${year} ${hours}:${minutes}:${seconds}`;
       }
    */

    // Calculate the index range for the current page
    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentItems = filteredData.slice(indexOfFirstItem, indexOfLastItem);

    const paginate = pageNumber => setCurrentPage(pageNumber);

    // Filter code 
    const [startDate, setStartDate] = useState(null);
    const [endDate, setEndDate] = useState(null);
    const handleFilterDate = (val) => (e) => {

        if (val == 'Start Date') {
            setStartDate(new Date(e.target.value).toISOString().split("T")[0]);
        }
        else {
            setEndDate(new Date(e.target.value).toISOString().split("T")[0]);
        }
    }

    const filterDataByDate = (start, end) => {
        if ((startDate != null) && (endDate != null)&&(startDate <= endDate)) {
            // Filter data based on date range
            let newFilteredData = tableData.filter(item => {
                const itemDate = new Date(item.KYCDate).toISOString().split("T")[0];
                return ((itemDate >= startDate) && (itemDate <= endDate));
            });
            console.log('newFilteredData--', newFilteredData)

            // Update the state with the filtered data and reset to the first page
            setFilteredData(newFilteredData);
            setCurrentPage(1);
        }
        else{
            alert("Enter Correct Date")
        }
    };

    const [SearchKycNumber, setSearchKycNumber] = useState(null);
    const handleFilterKYCNumber = (e) => {
        let newFilteredData;
        if (SearchKycNumber != '') {
            // Filter the original data based on a partial match with "KYCTypeDesc"
            newFilteredData = tableData.filter(item => item.KYCNumber?.toLowerCase().includes(SearchKycNumber.toLowerCase()));
        }
        else {
            newFilteredData = tableData;
        }
        // Update the state with the filtered data and reset to the first page
        setFilteredData(newFilteredData);
        setCurrentPage(1);
    }

    useEffect(() => {
        setTableData(getData)       //replace this getData only
        setFilteredData(getData)
    }, [])
    return (
        <>
            <div style={{ display: 'flex' }}>

                <label>Report Filter :</label>

                <select className="form-select"
                    onChange={(e) => {
                        setshowFilter(e.target.value)
                        setFilteredData(tableData)
                    }}>

                    <option value="" selected>--Select--</option>
                    <option value="allRecord">All Record</option>
                    <option value="dateWise">Date Wise</option>
                    <option value="riskType">RiskType</option>
                    <option value="kycNumber">Kyc Number</option>
                </select>
            </div>

            {showFilter == 'allRecord' ? (
                <>
                </>
            ) : (null)}
            {showFilter == 'dateWise' ? (
                <>
                    <div style={{ display: 'flex' }}>
                        <div>
                            <label for="filter_row">Start Date:</label>
                            <input type="date" className="form-control" onChange={handleFilterDate('Start Date')} placeholder="Filter Start Date" />
                        </div>
                        <div>
                            <label for="filter_row">End Date:</label>
                            <input type="date" className="form-control" onChange={handleFilterDate('End Date')} placeholder="Filter End Date" />
                        </div>
                        <div>
                            <button type="submit" className="btn" onClick={filterDataByDate}  >Filter record</button>
                        </div>
                    </div>
                </>
            ) : (null)}
            {showFilter == 'kycNumber' ? (
                <>
                    <div style={{ display: 'flex' }}>
                        <div>
                            <label for="filter_row">Kyc Number:</label>
                            <input type="text" className="form-control" onChange={(e) => {
                                setSearchKycNumber(e.target.value)
                            }}
                                placeholder="Filter Kyc Number" />
                        </div>
                        <div>
                            <button type="submit" className="btn" onClick={handleFilterKYCNumber}  >Filter record</button>
                        </div>
                    </div>
                </>
            ) : (null)}
            {showFilter == 'riskType' ? (
                <div style={{ display: 'flex' }}>
                    <label>Risk Type Filter :</label>
                    <select className="form-select"
                        onChange={(e) => {
                            let newFilteredData;
                            if (e.target.value != '') {
                                // Filter the original data based on a partial match with "KYCTypeDesc"
                                newFilteredData = tableData.filter(item => item.RiskTypeID == e.target.value);
                            }
                            else {
                                newFilteredData = tableData;
                            }
                            // Update the state with the filtered data and reset to the first page
                            setFilteredData(newFilteredData);
                            setCurrentPage(1);
                        }}>
                        <option value="" selected>--Select--</option>
                        <option value="3">Low</option>
                        <option value="2">Medium</option>
                        <option value="1">High</option>

                    </select>
                </div>
            ) : (null)}

            <div style={{ padding: '0 4%' }}>
                <table id="customers" class="table table-striped">
                    <thead style={{ background: '#009efb', color: 'white' }}>
                        <tr style={{ fontSize: 'medium' }}>
                            <th scope="col">Sr. no.</th>
                            <th scope="col">KYC Date</th>
                            <th scope="col">KYC Type</th>
                            <th scope="col">KYC Number</th>
                            <th scope="col">Customer Name</th>
                            <th scope="col">Account Type</th>
                            <th scope="col">Risk Type</th>
                            <th scope="col">Head Office</th>
                            <th scope="col">Branch Office</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {currentItems.map((item, index) => (
                            <tr>
                                <td scope="row">{index + 1}</td>
                                <td scope="row">{formatDate((item.KYCDate))}</td>
                                <td>{item.KYCTypeDesc}</td>
                                <td>{item.KYCNumber}</td>
                                <td>{item.FirstName} {item.LastName}</td>
                                <td>{item.AccountTypeDesc}</td>
                                <td>{item.RiskTypeDesc}</td>
                                <td>{item.CompanyDesc}</td>
                                <td>{item.BuDesc}</td>
                                <td>{item.StatusDesc}</td>
                                <td>
                                    <span className="material-symbols-outlined"> visibility </span>
                                    <span className="material-symbols-outlined">  edit </span>

                                </td>
                            </tr>
                        ))}

                    </tbody>
                </table>
                {/* Pagination */}
                <div style={{ textAlign: 'right', margin: '6px' }}>
                    <label for="filter_row">Row per page: &nbsp;</label>
                    <select style={{ border: 'none' }} name="filter_row" id="filter_row" onChange={(e) => setItemsPerPage(e.target.value)}>
                        <option value="10">10</option>
                        <option value="15" selected>15</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                    </select>&nbsp;
                    <button style={{ border: 'none', borderRadius: '50%', padding: "6px 10px" }} onClick={() => paginate(currentPage - 1)} disabled={currentPage === 1}> &lt;</button>
                    <span> Page {currentPage} </span>
                    <button style={{ border: 'none', borderRadius: '50%', padding: "6px 10px" }} onClick={() => paginate(currentPage + 1)} disabled={indexOfLastItem >= filteredData.length}> &gt; </button>
                </div>
            </div>
        </>
    );
}

export default Table;
