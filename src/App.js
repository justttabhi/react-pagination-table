import logo from './logo.svg';
import './App.css';
import Table from '../src/pages/table';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <div>
        React Table
        </div>
      </header>
      <Table/>
    </div>
  );
}

export default App;
